#pragma once

// standard library includes

// engine includes
#include "Engine/ComponentFactory.h"

namespace Kelvin
{
	// forward declarations

	// file code
	struct COMPONENT_NAMEResource;
	class COMPONENT_NAME;
	using COMPONENT_NAMEArgs = ComponentFactoryArguments<COMPONENT_NAME, COMPONENT_NAMEResource, MPL::Vector<>, MPL::Vector<>>;
	extern template class ComponentFactory<COMPONENT_NAMEArgs>;
	using COMPONENT_NAMEFactory = ComponentFactory<COMPONENT_NAMEArgs>;

	struct COMPONENT_NAMEResource
	{
		void VisitLiveEditableVariables(VariableVisitor& _visitor){}
		void VisitInitTimeVariables(VariableVisitor& _visitor){}
	};

	class COMPONENT_NAME
	{
		using Factory = COMPONENT_NAMEFactory;
		using Traits = Factory::Traits;
	public:
		COMPONENT_NAME(Traits::ConstructionParameters const& _parameters);
		~COMPONENT_NAME();
		ComponentExecutionResult Execute(Traits::ExecutionParameters const& _parameters);
	};
}
