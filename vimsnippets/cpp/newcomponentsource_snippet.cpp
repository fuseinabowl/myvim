// owning header include
#include "src/Components/COMPONENT_NAME.h"

// standard library includes

// engine includes

namespace Kelvin
{
	// file code
	
	template <> char const* GetComponentIdentifier<COMPONENT_NAMEArgs>() {return "COMPONENT_NAME";}
	template class ComponentFactory<COMPONENT_NAMEArgs>;

	COMPONENT_NAME::COMPONENT_NAME
	(
		Traits::ConstructionParameters const& _parameters
	)
	{
	}

	COMPONENT_NAME::~COMPONENT_NAME() = default;

	ComponentExecutionResult COMPONENT_NAME::Execute
	(
		Traits::ExecutionParameters const& _parameters
	)
	{
		ComponentExecutionResult result;
		return result;
	}
}
