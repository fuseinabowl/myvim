" how I like my vim set up
set tabstop=4
set shiftwidth=4
let mapleader = "-"
let maplocalleader = "\\"
set scrolloff=5
set splitbelow

" get out of insert mode without leaving the keyboard or making a chord
inoremap jk <esc>
" force myself to learn this new map
inoremap <esc> <nop>

" allow editing of this file
nnoremap <leader>ev :tabe $MYVIMRC<CR><C-w>r
nnoremap <leader>sv :source $MYVIMRC<CR><CR>

" quick window switch
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
" force use of new quick commands
nnoremap <C-w>h <nop>
nnoremap <C-w>j <nop>
nnoremap <C-w>k <nop>
nnoremap <C-w>l <nop>

" git commands
" git add (current file)
function! GitAddCurrentFile()
	silent execute "!git add " . expand('%')
	redraw!
	echom expand('%') . " added to git"
endfunc
nnoremap <silent> <leader>ga :call GitAddCurrentFile()<CR>
" git commit (all changes)
function! GitCommit()
	" don't need to do status here as git commit will do that for us
	execute "!git commit -v"
endfunc
nnoremap <silent> <leader>gc :call GitCommit()<CR>
nnoremap <silent> <leader>gs :! git commit <C-R>%<CR>

" helpful cpp snippets

function! CreateCppCommands()
	" indenting
	set cindent

	" template helpers
	iabbrev <buffer> tem template
	iabbrev <buffer> ty typename
	" template helper forcers
	" template can be written in other places
	" iabbrev <buffer> template template PLZNO
	iabbrev <buffer> typename typename PLZNO

	" return helper
	iabbrev <buffer> re return
	iabbrev <buffer> ret return
	" return helper forcers
	iabbrev <buffer> return return PLZNO

	" class keyword helpers
	iabbrev <buffer> cl class
	iabbrev <buffer> str struct
	" class keyword forcers
	" iabbrev <buffer> class class PLZNO
	" iabbrev <buffer> struct struct PLZNO

	" class definition helper
	iabbrev <buffer> strd struct<CR>{<CR>};<up><up><end>
	iabbrev <buffer> cld class<CR>{<CR>};<up><up><end>

	" typedef
	iabbrev <buffer> td typedef
	" typedef forcer
	iabbrev <buffer> typedef typedef PLZNO

	" triple comment makes a line of 80 slashes
	iabbrev <buffer> /// ////////////////////////////////////////////////////////////////////////////////

	" function definition
	iabbrev <buffer> func <CR>(<CR><BS>)<CR>{<CR> <BS><CR>}<up>

	" nullpointer
	iabbrev <buffer> nul nullptr
	iabbrev <buffer> nult nullptr_t

	" include helping 
	" helper style 1:
	"	(put here rather than header specific in case I need to make SCUs)
	"	invoke on one file to say "I want to reference this file"
	"	invoke on one or more files to say "insert reference here"
	"	helper will work out path
	" helper style 2:
	"	invoke on one class that has the same name as its containing file
	"	searches for the file and, if vim finds it, will add it to the engine includes section
	" helper style 3:
	"	invoke anywhere for std include
	"	enter the header name, if it doesn't exist it is added to the standard library includes section
	" helper style 4:
	"	same as style 3, but for engine includes
endfunction

function! StoreThisFileReference()
	let s:currentFilePath = expand('%')
endfunction

function! PutCurrentFileReferenceAbsolute()
	PutFileReferenceAbsolute(s:currentFilePath)
endfunction

function! PutFileReferenceAbsolute(filePath)
	" find where to put the reference in relation to the current working directory
	let workingDirectory=pwd
	" ensure common base
	if match(l:workingDirectory, a:filePath)
		" strip the working directory from the filepath
		" put it at the engine includes mark in the file
		execute "normal! 'eo#include <\<c-r>=l:strippedFilePath\<CR>>\<CR>``"
	else
		" let the user know that the include was not able to complete
	endif
endfunction

function! CreateForwardDeclaration(declarationType)
	" copy the symbol under the cursor
	let symbolName = expand("<cword>")
	let declarationString = a:declarationType . " " . l:symbolName . ";"

	" goto the forward declarations and paste it there, along with the declaration type
	" then return to the user's cursor position
	execute "normal! 'fo\<c-r>=l:declarationString\<CR>\<esc>``"

	" let the user what they just did
	echom a:declarationType . " " . l:symbolName . " > forward declared"
endfunction

function! CreateImplementation()
	" copy the range between the start of this definition (the first non-whitespace character
	" after the previous semicolon) and the end of this definition (the last non-whitespace character
	" before the next semicolon)

	" goto previous semicolon
	" goto next non-whitespace character
	" read from this point to the next semicolon
	" remove semicolon and any trailing whitespace

	" polish - find previous function definition's name
	" put after this function in the source file too

	" goto source file
	" put contents of string, along with classname before function name
	" add parentheses after function signature
endfunction

function! CreateCppHeaderCommands()
	execute "nnoremap <buffer> \<leader>fc :call CreateForwardDeclaration(\"class\")\<CR>"
	execute "nnoremap <buffer> \<leader>fs :call CreateForwardDeclaration(\"struct\")\<CR>"
endfunction

" in general, these include the snippet, then replace all "FILENAME"s with the actual filename
" they will end on ENDMARKER (which is deleted)

function! DeleteBlankTopLine()
	normal! ggdd
endfunction

function! ReplaceFilenamePlaceholders()
	" replace all FILENAMEs with the actual filename
	let filename = expand('%:t:r')
	"echom filename
	let command = "%s/FILENAME/" . filename . "/g"
	"echom command
	execute command
endfunction

function! NewHeader()
	r~/vimsnippets/cpp/newheader_snippet.cpp"
	"call ReplaceFilenamePlaceholders()
	call DeleteBlankTopLine()

	" mark places to put includes and other automated inserts
	" s: standard library headers
	execute "normal! /standard library includes\<CR>ms"
	" e: engine headers
	execute "normal! /engine includes\<CR>me"
	" f: forward declares
	execute "normal! /forward declarations\<CR>mf"
endfunction

function! NewCppSource()
	r~/vimsnippets/cpp/newsource_snippet.cpp"
	call ReplaceFilenamePlaceholders()
	call DeleteBlankTopLine()

	" mark places to put includes and other automated inserts
	" s: standard library headers
	execute "normal! /standard library includes\<CR>ms"
	" e: engine headers
	execute "normal! /engine includes\<CR>me"
endfunction

augroup CPP
	" remove previous bindings from this group
	autocmd!

	" add boilerplate when creating new files
	autocmd BufNewFile *.h,*.inl :call NewHeader()
	autocmd BufNewFile *.cpp :call NewCppSource()
	" move the cursor to ENDMARKER and delete it
	autocmd BufNewFile *.h,*.inl,*.cpp /ENDMARKER
	autocmd BufNewFile *.h,*.inl,*.cpp :normal! diw
	autocmd FileType cpp :call CreateCppCommands()

	" create forward declaration of a class under the cursor
	" not a filetype as can't determine between headers and source (which doesn't have forward declarations)
	autocmd BufRead,BufNewFile *.h,*.inl :call CreateCppHeaderCommands()
	autocmd BufRead,BufNewFile *.h,*.inl 

	" "git add" the file on saving it
	autocmd BufWritePost *.h,*.inl,*.cpp,*.am :call GitAddCurrentFile()
augroup END

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" highlight all instances of the word under the cursor
function! HighlightWordUnderCursor()
	silent! execute "normal! :let @/ = \"\\\\<\<C-r>\<C-w>\\\\>\"\<CR>"
endfunction

" toggle highlighting on or off for this buffer
function! StartHighlightingWordsUnderCursor()
	augroup Highlighting
		" remove previous bindings from this group
		autocmd!

		" find it a little annoying now
		" autocmd CursorMoved * :call HighlightWordUnderCursor()
	augroup END

	" set the colour to a less intrusive one
	highlight Search guifg=white guibg=NONE cterm=NONE ctermfg=white ctermbg=NONE

	call HighlightWordUnderCursor()
endfunction

function! StopHighlightingWordsUnderCursor()
	" clear the search buffer
	let @/ = ""

	" stop highlighting commands
	autocmd! Highlighting

	" set the colour to a more noticable one
	highlight Search guifg=black guibg=yellow cterm=NONE ctermfg=black ctermbg=yellow
endfunction

" all search functions will interfere with word highlighting - calling them
" will disable word highlighting until the user reenables it
"noremap <silent> /      :call StopHighlightingWordsUnderCursor()<CR>/
"noremap <silent> ?      :call StopHighlightingWordsUnderCursor()<CR>?
"noremap <silent> *      :call StopHighlightingWordsUnderCursor()<CR>*
"noremap <silent> #      :call StopHighlightingWordsUnderCursor()<CR>#
"noremap <silent> v      :call StopHighlightingWordsUnderCursor()<CR>v
"noremap <silent> <S-v>  :call StopHighlightingWordsUnderCursor()<CR><S-v>
"noremap <silent> <C-v>  :call StopHighlightingWordsUnderCursor()<CR><C-v>

" restart highlighting with space
"nnoremap <silent> <space> :call StartHighlightingWordsUnderCursor()<CR>
nnoremap <silent> <space> :set hlsearch! hlsearch?<CR>

"call StartHighlightingWordsUnderCursor()
set hlsearch

"find all in project
nnoremap <silent> <F4> :execute "grep " . expand("<cword>") . " -r --include \*.h --include \*.cpp --include \*.inl *" <Bar> cw<CR>

"opening files opens them in a new window
"nnoremap gf <C-w>gf

command! M wa | make! -j10

map <C-o> :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>

function! CreateComponent(name)
	let l:file_path_no_extension = "src/Components/" . a:name
	execute! "!cp ~/vimsnippets/cpp/newcomponentheader_snippet.cpp " l:file_path_no_extension . ".h"
	execute! "e " . l:file_path_no_extension . ".h"
	execute! "%s/COMPONENT_NAME/" . a:name . "/g"

	execute! "!cp ~/vimsnippets/cpp/newcomponentsource_snippet.cpp " l:file_path_no_extension . ".cpp"
	execute! "split " . l:file_path_no_extension . ".cpp"
	execute! "%s/COMPONENT_NAME/" . a:name . "/g"
endfunction

function! CopyIncludablePath()
	let l:file_path_no_src = expand("%:.:s?src/??")
	let @" = "#include \"" . l:file_path_no_src . "\"\n"
endfunction

nnoremap <silent> <leader>ci :call CopyIncludablePath()<cr>

function! GetTextBetweenTwoPos(start, end)
	let l:all_lines_list = getline(a:start[1], a:end[1])
	let l:all_lines_string = join(l:all_lines_list, '')
	let l:text_begin_cropped = l:all_lines_string[a:start[2]:]
	let l:end_line = getline(a:end[1])
	let l:end_line_length = strlen(l:end_line)
	let l:text_fully_cropped = l:text_begin_cropped[:l:end_line_length - a:end[2] - 2]
	return l:text_fully_cropped
endfunction

function! GetFunctionSignatureUnderCursor()
	let l:cursor_save = getpos(".")

	call search(';\|private:\|public:\|protected:', 'be')
	let l:start_of_signature_pos = getpos(".")
	call search(";")
	let l:end_of_signature_pos = getpos(".")

	return GetTextBetweenTwoPos(l:start_of_signature_pos, l:end_of_signature_pos)
endfunction

function! RemoveFunctionDeclarationDecorationNotNeededForDefinition(function_header_signature)
	let l:removed_leading_cruft = substitute(a:function_header_signature, '\(\s\|static\)*', '', '')
	let l:removed_trailing_semicolon = substitute(l:removed_leading_cruft, ';', '', '')
	return l:removed_trailing_semicolon
endfunction

function! FindClassNameForNamespaceUnderCursor()
	call searchpair('{', '', '}', 'bW')
	let l:found_opening_brace = getpos('.')
	call search('class\|struct', 'beW')
	let l:found_class_declaration_start = getpos('.')
	let l:class_name_with_spacing = GetTextBetweenTwoPos(l:found_class_declaration_start, l:found_opening_brace)
	let l:class_name = substitute(l:class_name_with_spacing, '\s*', '', 'g')
	return l:class_name
endfunction

function! DecorateFunctionSignatureWithClass(current_function_signature, owning_class_name)
	return substitute(a:current_function_signature, '\w\+(', a:owning_class_name . '::\0', '')
endfunction

function! SeparateFunctionSignatureArguments(current_function_signature)
	let l:leading_signature_with_separated_opening_bracket = substitute(a:current_function_signature, '\(.\{-}\)(.*', '\1\n\t(', '')
	let l:arguments = substitute(a:current_function_signature, '.\{-}(\(.\{-}\)).*', '\1', '')
	let l:trailing_signature = substitute(a:current_function_signature, '.\{-}(.\{-}\().*\)', '\1', '')

	let l:arguments_split = split(l:arguments, '\s*,\s*', 'g')
	let l:accumulated_arguments_with_indents = join(l:arguments_split, ",\n\t\t")
	if strlen(l:accumulated_arguments_with_indents) > 0
		let l:accumulated_arguments_with_indents = "\n\t\t" . l:accumulated_arguments_with_indents
	endif
	return l:leading_signature_with_separated_opening_bracket . l:accumulated_arguments_with_indents . "\n\t" . l:trailing_signature
endfunction

function! CopyFunctionImplementationStub()
	let l:cursor_save = getpos(".")

	let l:function_signature = GetFunctionSignatureUnderCursor()
	let l:definition_function_signature = RemoveFunctionDeclarationDecorationNotNeededForDefinition(l:function_signature)
	let l:owning_class_name = FindClassNameForNamespaceUnderCursor()
	let l:class_decorated_function_signature = DecorateFunctionSignatureWithClass(l:definition_function_signature, l:owning_class_name)
	let l:fully_decorated_function_signature = SeparateFunctionSignatureArguments(l:class_decorated_function_signature)
	let @" = "\n\t" . l:fully_decorated_function_signature . "\n\t{\n\t}\n"

	call setpos(".", l:cursor_save)
endfunction

nnoremap <silent> <leader>cf :call CopyFunctionImplementationStub()<cr>
